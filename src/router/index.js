import { createRouter, createWebHashHistory } from 'vue-router';
import todoApp from '@/views/todoApp.vue';
import todoGeneral from '@/views/todoGeneral.vue';

const routes = [
  {
    mame: 'todoGeneral',
    component: todoGeneral,
    path: '/',
  },
  {
    name: 'todoList',
    component: todoApp,
    path: '/:key',
  },
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

export default router;
